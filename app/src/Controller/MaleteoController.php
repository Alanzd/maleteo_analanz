<?php

    namespace App\Controller;

use App\Entity\Maleteo;
use App\Form\MaleteoFormType;
use Doctrine\Common\Annotations\Annotation\Enum;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


    class MaleteoController extends AbstractController{

    /**
     * @Route("/", name="listar_maleteo") //es la ruta de la funcion del formulario
     */


    public function maleteo(Request $request,EntityManagerInterface $em)  //esta funcion me va a crear un formulario, necesitamos la request, cogemos el objeto request y se lo pasamos a nuestro metodo
    {
        $form = $this->createForm(MaleteoFormType::class);
        //crea un objeto tipo formulario

        $form->handleRequest($request);
        // el formulario tiene que gesionar la request

        if($form->isSubmitted() && $form->isValid()){
            //si el form es valido
            $maleteo = $form->getData();
            //objeto de tipo solicitud

                $em->persist($maleteo); //persiste la solicitud
                $em->flush($maleteo); //guardala en la bbdd

                return $this->redirectToRoute('listar_maleteo');
                //redirigir a ruta listar_maleteo

                }


        return $this->render('maleteo/index.html.twig',
        //lo pinta en la plantilla twig
        [
            'DemoForm' => $form->createView(),
        //meto una variable cuyo valor va a ser la llamada a un metodo que genera la vista del formulario
        ]);

    }

}


?>
