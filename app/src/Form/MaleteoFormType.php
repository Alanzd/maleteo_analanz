<?php

namespace App\Form;

use App\Entity\Maleteo;
use App\Entity\Solicitud;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaleteoFormType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        //definimos los campos del formulario
        $builder->add('nombre');
        $builder->add('email', EmailType::class);
        $builder->add('ciudad', ChoiceType::class,
    [
            'choices' => [
                'Madrid' => 'Madrid',
                'Valencia' => 'Valencia',
                'Barcelona' => 'Barcelona'
            ],
            'placeholder' =>'Elige una opcion'
    ]);
        $builder->add ('privacidad', CheckboxType::class,
        [
            'label' => 'Acepto la politica de privacidad',
            'required' => 'true'
        ]); 
        $builder->add('Enviar', SubmitType::class);
    }
        public function configureOptions(OptionsResolver $resolver) //Para manejar los datos del formulario de manera dinamica
        {
            $resolver->setDefaults(['data_class' => Maleteo::class]);
            
        }

    

}